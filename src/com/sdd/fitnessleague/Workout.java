package com.sdd.fitnessleague;

import java.util.ArrayList;

/**
 * This class is a Singleton class that provides internal representation 
 * of the workout.
 * @author willis4
 *
 */
public class Workout {
	private static Workout instance = new Workout();
	private int currentExercise;
	private ArrayList<String> exerciseOrder;
	
	/**
	 * Initializes variables
	 */
	private Workout(){
		currentExercise = 0;
		exerciseOrder = new ArrayList<String>();
	}
	
	/**
	 * Gets the current workout instance
	 * @return The current workout instance
	 */
	public static Workout getInstance(){
		return instance;
	}
	
	/**
	 * Loads a list of exercises into the current workout
	 * @param e The exercises to be added
	 */
	public void loadExerciseList(ArrayList<String> e){
		instance.exerciseOrder.clear();
		instance.exerciseOrder.addAll(e);
		currentExercise = 0;
	}
	
	/**
	 * Starts the execution of a new workout
	 * @return The name of the first workout to be completed
	 */
	public String start(){
		currentExercise = 0;
		if (exerciseOrder.size() > 0 && exerciseOrder.size() > currentExercise){
			return exerciseOrder.get(currentExercise++);
		}
		return "";
	}
	
	/**
	 * Returns the next exercise to be completed in the workout
	 * @return The next exercise to be completed
	 */
	public String getNextExercise(){
		if (exerciseOrder.size() > 0 && exerciseOrder.size() > currentExercise){
			return exerciseOrder.get(currentExercise++);
		}
		return "";
	}
	
	/**
	 * Checks if the workout is complete
	 * @return true if the workout has ongoing, false otherwise
	 */
	public boolean isActive(){
		return (exerciseOrder.size() > currentExercise && exerciseOrder.size() > 0);
	}
}

