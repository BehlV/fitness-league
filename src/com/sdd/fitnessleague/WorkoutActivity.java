package com.sdd.fitnessleague;

import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * This class controls the workout view and allows the user to start a new workout
 * @author willis4
 *
 */
public class WorkoutActivity extends BaseActivity {
	private String workoutName = "";
	private ListView listview;
	private ArrayList<String> exercises;

	/**
	 * Creates a new workout manager 
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_workout);
		
		Intent intent = getIntent();
		if (intent != null)
			workoutName = intent.getStringExtra(MainActivity.WORKOUT_SELECTED);
		
		setupActionBar();
		
		//Retrieves the list of exercises to display
		setExerciseList();
		listview = (ListView) findViewById(R.id.workout_view);
		
	    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_checked, exercises);
	    listview.setAdapter(adapter);  
        listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
	}
	
	/**
	 * Displays the title as the workout name
	 */
	protected void setupActionBar() {
		super.setupActionBar();
		getActionBar().setDisplayShowTitleEnabled(true);
		getActionBar().setTitle(workoutName);
	}
	
	/**
	 * Talks to the database in order to generate a list of exercise for the workout
	 */
	private void setExerciseList(){
		WorkoutManagerTask wmt = new WorkoutManagerTask();
		try {
			exercises = wmt.execute("GetWorkoutByName", workoutName, uId).get();
			if (exercises == null)
			{
				exercises = new ArrayList<String>();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Starts the selected workout
	 */
	public void startWorkout(View view){
		 SparseBooleanArray positions = listview.getCheckedItemPositions();

		 ArrayList<String> selectedList = new ArrayList<String>();
		 for (int i = 0;  i < exercises.size(); i++){
			// Adds the exercise if it has been selected by the UI
			 if (positions.get(i))
			 {
				 selectedList.add(exercises.get(i));
			 }
		 }
        
		//Load the subset of exercises into the workout class
		Workout w = Workout.getInstance();
		w.loadExerciseList(selectedList);

	    if (w.isActive()){
			Intent intent = new Intent(this, ExerciseActivity.class);
	        intent.putExtra(MainActivity.EXERCISE_SELECTED, w.start());
	        startActivity(intent);
	    }
	}
}
