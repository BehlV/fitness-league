package com.sdd.fitnessleague;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;


import java.util.ArrayList;


/**
* This class allows for the execution of network communication to the database
 * for logging exercises in a separate thread.
 * @author willis4
 *
 */
public class LogManagerTask extends AsyncTask<String, Void, ArrayList<String>>{

	@Override
	/**
	 * Function that will be perfomed in a separate task
	 * @return rv ArrayList<String> represents the data that has been collected or 
	 * the return value of an edit function
	 */
	protected ArrayList<String> doInBackground(String... params) {
		ArrayList<String> rv= new ArrayList<String>();
		try {
			//Add a new activity to the log
			if (params[0].equals("EditLog")) {
				Integer r = LogManager.AddCompleteExercise(params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8]);
				rv.add(r.toString());
			}
			//Get the complete log for a specified exercise formatted in a string
			else if (params[0].equals("GetLogString")) {
				JSONArray jsonList =  LogManager.GetCompletedExercise(params[1], params[2]);
        		for (int i = 0; jsonList != null && i < jsonList.length(); i++) {
        			JSONObject jsonObj = jsonList.getJSONObject(i);
    				if (jsonObj != null && jsonObj.has("logdate")&& jsonObj.has("exerciseID")&& jsonObj.has("weight1") &&
    						jsonObj.has("rep1")&& jsonObj.has("weight2")&& jsonObj.has("rep2")&& jsonObj.has("weight3") &&
    						jsonObj.has("rep3")){
    					String log = jsonObj.getString("logdate") + "\nexercise :" + jsonObj.getString("exerciseID") + "\nWeight:" + jsonObj.getString("weight1") + 
    							"    Reps:" +jsonObj.getString("rep1") + "\nWeight:" + jsonObj.getString("weight2") + "    Reps:" + jsonObj.getString("rep2") + 
    							"\nWeight:" +jsonObj.getString("weight3") + "    Reps:" + jsonObj.getString("rep3");
    					rv.add(log);
    				}
    				else {
    					rv.add("");
    				}
        		}
			}
			//Get the newest log for an exercise
			else if (params[0].equals("GetRecentLog")) {
				JSONArray jsonList =  LogManager.GetCompletedExercise(params[1], params[2]);
				if (jsonList != null && jsonList.length() > 0){
					// The most recent log is listed last
        			JSONObject jsonObj = jsonList.getJSONObject(jsonList.length()-1);
    				if (jsonObj != null && jsonObj.has("logdate")&& jsonObj.has("exerciseID")&& jsonObj.has("weight1") &&
    						jsonObj.has("rep1")&& jsonObj.has("weight2")&& jsonObj.has("rep2")&& jsonObj.has("weight3") &&
    						jsonObj.has("rep3")){
    					rv.add(jsonObj.getString("logdate"));
    					rv.add(jsonObj.getString("exerciseID"));
    					rv.add(jsonObj.getString("weight1"));
    					rv.add(jsonObj.getString("rep1"));
    					rv.add(jsonObj.getString("weight2"));
    					rv.add(jsonObj.getString("rep2"));
    					rv.add(jsonObj.getString("weight3"));
    					rv.add(jsonObj.getString("rep3"));
    				}
				}
				else {
					rv.add("");
				}
			}
			// Get all exercises that have been completed by the user and format to a string per log entry
			else if (params[0].equals("GetCompletedExerciseByUser")) {
				JSONArray jsonList =  LogManager.GetCompletedExerciseByUser(params[1]);
				if (jsonList != null && jsonList.length() > 0){
					for(int i = 0; i < jsonList.length(); i++)
					{
	        			JSONObject jsonObj = jsonList.getJSONObject(i);
	    				if (jsonObj != null && jsonObj.has("logdate")&& jsonObj.has("exerciseID")&& jsonObj.has("weight1") &&
	    						jsonObj.has("rep1")&& jsonObj.has("weight2")&& jsonObj.has("rep2")&& jsonObj.has("weight3") &&
	    						jsonObj.has("rep3")){
	    					String log = jsonObj.getString("logdate") + "\nexercise :" + jsonObj.getString("exerciseID") + 
	    							"\nWeight:" + jsonObj.getString("weight1") + "    Reps:" +jsonObj.getString("rep1") + 
	    							"\nWeight:" + jsonObj.getString("weight2") + "    Reps:" + jsonObj.getString("rep2") + 
	    							"\nWeight:" +jsonObj.getString("weight3") + "    Reps:" + jsonObj.getString("rep3");
	    					rv.add(log);
	    				}
					}
        		}
				else {
					rv.add("");
				}
			}
			return rv;
		}
		catch (Exception e) {
        	e.printStackTrace();
            return null;
		}
	}
}
