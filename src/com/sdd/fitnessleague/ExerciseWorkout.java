package com.sdd.fitnessleague;
/**
 * A class to hold name of workout and if it is selected
 * @author Alvin Lee
 */
public class ExerciseWorkout {

	String code = "";
	 String name = null;
	 boolean selected = false;
	  
	 public ExerciseWorkout(String name, boolean selected) {
	  super();
	  this.name = name;
	  this.selected = selected;
	 }
	  
	 public String getCode() {
	  return code;
	 }
	 public void setCode(String code) {
	  this.code = code;
	 }
	 public String getName() {
	  return name;
	 }
	 public void setName(String name) {
	  this.name = name;
	 }
	 
	 public boolean isSelected() {
	  return selected;
	 }
	 public void setSelected(boolean selected) {
	  this.selected = selected;
	 }
	  
}
