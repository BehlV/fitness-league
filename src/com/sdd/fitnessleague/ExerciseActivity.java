package com.sdd.fitnessleague;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * The class displays information relating to a particular
 * exercise and allows the user to input specialized information.
 * @author willis4, leungc5
 *
 */
public class ExerciseActivity extends BaseActivity {
	private String eId = "";
	private String exerciseName = "";
	
	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	
	/**
	 * Method called upon creation that sets up the initial view
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_exercise);
		
		Intent intent = getIntent();
		if (intent != null)
			exerciseName = intent.getStringExtra(MainActivity.EXERCISE_SELECTED);
		setupActionBar();
		setExerciseData(exerciseName);
	}

	/**
	 * Overridden to allow for the title to be changed to a dynamic value
	 */
	protected void setupActionBar() {
		super.setupActionBar();
		getActionBar().setDisplayShowTitleEnabled(true);
		getActionBar().setTitle(exerciseName);
	}
	
	/**
	 * This method loads the data from the database to be displayed in the view
	 * @param name Name of the exercise to be displayed
	 */
	private void setExerciseData(String name)
	{
		ArrayList<String> result;
		try{
	    	ExerciseManagerTask emt = new ExerciseManagerTask();
	    	result = emt.execute(name).get();
	    	// If the QR code returned is unknown
	    	if (result.isEmpty() || result.size() < 3){
	    		Toast.makeText(this, "Exercise Unknown",Toast.LENGTH_SHORT).show();
	    		NavUtils.navigateUpFromSameTask(this);
	    	}
	    	// If the activity is known, set the description box appropriately
	    	else{
	    		TextView desc_box = (TextView) findViewById(R.id.activity_description);
	    		desc_box.setText(result.get(1));
	    		eId = result.get(2);
		    }
	    	
	    	LogManagerTask lmt = new LogManagerTask();
	    	result = lmt.execute(new String[]{"GetRecentLog", uId, eId}).get();
	    	
	    	// If a log was returned for the specified string
	    	// Log is in form [date exercise weight1 reps1 weight2 reps2 weight3 reps3]
	    	if (result.size() >= 8){
	    		EditText textbox = (EditText) findViewById(R.id.weight1);
	    		textbox.setText(result.get(2));
	    		textbox = (EditText) findViewById(R.id.reps1);
	    		textbox.setText(result.get(3));
	    		textbox = (EditText) findViewById(R.id.weight2);
	    		textbox.setText(result.get(4));
	    		textbox = (EditText) findViewById(R.id.reps2);
	    		textbox.setText(result.get(5));
	    		textbox = (EditText) findViewById(R.id.weight3);
	    		textbox.setText(result.get(6));
	    		textbox = (EditText) findViewById(R.id.reps3);
	    		textbox.setText(result.get(7));
	    		
	    	}
	    }catch (ExecutionException e){
			e.printStackTrace();
		}catch (InterruptedException e){
			e.printStackTrace();
			
		}
	}
	
	public void insertWorkoutToDatabase(View view){
		EditText wBox1 = (EditText) findViewById(R.id.weight1);
		EditText wBox2 = (EditText) findViewById(R.id.weight2);
		EditText wBox3 = (EditText) findViewById(R.id.weight3);
		
		String wBoxString1 = wBox1.getText().toString();
		String wBoxString2 = wBox2.getText().toString();
		String wBoxString3 = wBox3.getText().toString();
		
		EditText rep1 = (EditText) findViewById(R.id.reps1);
		EditText rep2 = (EditText) findViewById(R.id.reps2);
		EditText rep3 = (EditText) findViewById(R.id.reps3);
		
		String rBoxString1 = rep1.getText().toString();
		String rBoxString2 = rep2.getText().toString();
		String rBoxString3 = rep3.getText().toString();
		

		handleExpLvl();

		try{
			String[] input = new String[]{
					"EditLog", uId, eId, wBoxString1, wBoxString2, wBoxString3, rBoxString1, rBoxString2, rBoxString3
			};
			
			LogManagerTask lmt = new LogManagerTask();
			ArrayList<String> rv = lmt.execute(input).get();
			if (rv.size() == 1 && rv.get(0).contentEquals("0"))
			{
				Toast.makeText(this, "Log save failed.", Toast.LENGTH_LONG).show();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		//Proceed to next workout if a workout is ongoing
		Workout wkt = Workout.getInstance();
		if (wkt.isActive()){
			startActivity(wkt.getNextExercise());
		} else{
    		NavUtils.navigateUpFromSameTask(this);
		}
	}
	
	/**
	 * Starts the ExerciseActivity with the parameter of what exercise to start
	 * @param activityName The name of the exercise to be started
	 */
	protected void startActivity(String activityName)
	{
   		Intent intent = new Intent(this, ExerciseActivity.class);
        intent.putExtra(MainActivity.EXERCISE_SELECTED, activityName);
        startActivity(intent);
	}
	
	/**
	 * handleExpLvl gives experience to the user, determines if the user can level up,
	 * and if so it will add a level and reset the experience
	 */
	public void handleExpLvl() {		
		Session session = Session.getActiveSession();
		if (session != null && session.isOpened()) {
			Request.newMeRequest(session, new Request.GraphUserCallback() {

				@Override
				public void onCompleted(GraphUser user, Response response) {
					int currentExp = 0;
					int currentLevel = 0;
					
					if (user != null) {
						//Gets get current user experience and level
						try{
							LevelManagerTask emt = new LevelManagerTask();
							ArrayList<String> result = emt.execute("getExp", user.getId()).get();
							if (result.size() != 0 || result != null){
								currentExp = Integer.parseInt(result.get(0));								
							}
							
							LevelManagerTask emt2 = new LevelManagerTask();
							result = emt2.execute("getLevel", user.getId()).get();
							if (result.size() != 0 || result != null){
								currentLevel = Integer.parseInt(result.get(0));								
							}
							
							int nextLevelExp = (int)(10 * power(currentLevel));
							
							//If the add exp is greater than required exp, then level up
							if( (currentExp + 5) > nextLevelExp ) {
								String[] newLevel = new String[]{
									"setLevel",
									user.getId(),
									Integer.toString(currentLevel + 1)
								};
								LevelManagerTask emt3 = new LevelManagerTask();
								emt3.execute(newLevel);
								
								String[] newExp = new String[]{
									"setExp",
									user.getId(),
									Integer.toString((currentExp + 5) - nextLevelExp)
								};
								LevelManagerTask emt4 = new LevelManagerTask();
								emt4.execute(newExp);
								
								//Check for achievement completion
								LevelManagerTask lmt5 = new LevelManagerTask();
								result = lmt5.execute("getAchievement", "level", Integer.toString(currentLevel + 1)).get();
								if (result.size() != 0 || result != null){
									if(Integer.parseInt(result.get(0)) == 1){
										Toast.makeText(ExerciseActivity.this, "Congratulations on reaching level " + Integer.toString(currentLevel + 1) + "!", Toast.LENGTH_SHORT).show();
									}
								}
							}
							//Otherwise only increase current exp
							else {
								String[] newExp = new String[]{
									"setExp",
									user.getId(),
									Integer.toString((currentExp + 5))
								};
								LevelManagerTask emt3 = new LevelManagerTask();
								emt3.execute(newExp);
							}
						}
						catch (Exception e) {
							e.printStackTrace();
						}
					}
				}				
			}).executeAsync();
		}
	}
	
	/**
	 * Function to calculate the power with base of 1.10
	 * @param exp
	 * @return double 1.10^exp
	 */
	private double power(int exp) {
		if (exp >= 1)
			return 1.10 * (power(exp - 1));
		else
			return 1;
	}
	
	/**
	 * 
	 * @return message: the message that is going to get shared on Facebook
	 */
	private String feedMessage() {
		String message = ""; 
		int numReps = 0;
		
		EditText reps1 = (EditText) findViewById(R.id.reps1);
		EditText reps2 = (EditText) findViewById(R.id.reps2);
		EditText reps3 = (EditText) findViewById(R.id.reps3);
		
		try {
			if (reps1.getText().toString().isEmpty())
				reps1.setText("0");
			if (reps2.getText().toString().isEmpty())
				reps2.setText("0");
			if (reps3.getText().toString().isEmpty())
				reps3.setText("0");
			numReps = Integer.parseInt(reps1.getText().toString()) +
					  Integer.parseInt(reps2.getText().toString()) +
					  Integer.parseInt(reps3.getText().toString());
		}
		catch(Exception e) {
			numReps = 0;
		}
		
		message = "I did " + numReps + " reps of " + exerciseName;
		
		return message;				
	}
	
	public void publishStory(View view) {
	    Session session = Session.getActiveSession();

	    if (session != null){

	        // Check for publish permissions    
	        List<String> permissions = session.getPermissions();
	        if (!isSubsetOf(PERMISSIONS, permissions)) {
	            Session.NewPermissionsRequest newPermissionsRequest = new Session
	                    .NewPermissionsRequest(this, PERMISSIONS);
	        session.requestNewPublishPermissions(newPermissionsRequest);
	            return;
	        }

	        String message = feedMessage();
	        
	        Bundle postParams = new Bundle();
	        postParams.putString("message", message + ", and recorded it in my FitnessLeague app!");
	        //postParams.putString("name", "Facebook SDK for Android");
	        //postParams.putString("caption", "Build great social apps and get more installs.");
	        //postParams.putString("description", "The Facebook SDK for Android makes it easier and faster to develop Facebook integrated Android apps.");
	        //postParams.putString("link", "https://developers.facebook.com/android");
	        //postParams.putString("picture", "https://raw.github.com/fbsamples/ios-3.x-howtos/master/Images/iossdk_logo.png");

	        Request.Callback callback= new Request.Callback() {
	            public void onCompleted(Response response) {
	                JSONObject graphResponse = response
	                                           .getGraphObject()
	                                           .getInnerJSONObject();
	                try {
	                	graphResponse.getString("id");
	                } catch (JSONException e) {
	                    
	                }
	                FacebookRequestError error = response.getError();
	                if (error != null) {
	                    Toast.makeText(ExerciseActivity.this, error.getErrorMessage(), Toast.LENGTH_SHORT).show();
	                    } 
	                    else {
	                        Toast.makeText(ExerciseActivity.this, "Message Posted", Toast.LENGTH_SHORT).show();
	                }
	            }
	        };

	        Request request = new Request(session, "me/feed", postParams, 
	                              HttpMethod.POST, callback);

	        RequestAsyncTask task = new RequestAsyncTask(request);
	        task.execute();
	    }

	}
	
	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
	    for (String string : subset) {
	        if (!superset.contains(string)) {
	            return false;
	        }
	    }
	    return true;
	}
}
