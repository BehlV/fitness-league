package com.sdd.fitnessleague;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * This class allows for the execution of network communication to the database
 * for workout tasks in a separate thread.
 * @author willis4
 *
 */
public class WorkoutManagerTask extends AsyncTask<String, Void, ArrayList<String>> {
	    
	/**
	 * Function that will be performed in a separate task
	 * @return requestedStrings ArrayList<String> represents the data being collected
	 *  or null if adding an workout or of the exercise is not found.
	 */
	    protected ArrayList<String> doInBackground(String... params) {
	        ArrayList<String> requestedStrings = new ArrayList<String>();
        	try {
        		// Keyword to add a new workout to the list
    			//Params : accountId workout_name
        		if (params[0].equals("AddWorkout"))
        		{
        			requestedStrings.add(String.valueOf(WorkoutManager.addWorkout(params[1], params[2])));
        		}
        		// Keyword to add a new exercise to an existing workout
    			//Params : workoutName exerciseId accountId
        		else if (params[0].equals("AddExerciseToWorkout"))
	        	{
        			requestedStrings.add(String.valueOf(WorkoutManager.addExerciseToWorkout(params[1], params[2], params[3])));
	        	}
        		// Keyword to get a particular workout by name
        		// Params : workoutName, accountId
        		else if (params[0].equals("GetWorkoutByName"))
	        	{
        			 JSONArray jsonList = WorkoutManager.getWorkoutByName(params[1], params[2]);
        			 for (int i = 0; jsonList != null && i < jsonList.length(); i++) {
  	        			JSONObject jsonObj = jsonList.getJSONObject(i);
  	        			if (jsonObj != null)
  	        				requestedStrings.add(jsonObj.getString("name"));
  	        		}
	        	}
        		// Keyword to get all workouts for a user
        		// Params : accountId
        		else if (params[0].equals("listAll"))
	        	{
        			JSONArray jsonList = WorkoutManager.getAllWorkouts(params[1]);
        			 for (int i = 0; jsonList != null && i < jsonList.length(); i++) {
 	        			JSONObject jsonObj = jsonList.getJSONObject(i);
 	        			if (jsonObj != null)
 	        				requestedStrings.add(jsonObj.getString("Name"));
 	        		}
	        	}
	            return requestedStrings;
	        } catch (Exception e) {
	        	e.printStackTrace();
	            return null;
	        }
	    }
}
