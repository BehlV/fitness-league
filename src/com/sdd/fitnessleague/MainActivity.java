package com.sdd.fitnessleague;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dm.zbar.android.scanner.ZBarConstants;
import com.dm.zbar.android.scanner.ZBarScannerActivity;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import net.sourceforge.zbar.Symbol;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;
import java.util.Date;
import java.util.Locale;

/**
 * This class controls the main page of the application.
 */
public class MainActivity extends BaseActivity {

	private static final int ZBAR_SCANNER_REQUEST = 0;
    private static final int ZBAR_QR_SCANNER_REQUEST = 1;
	public final static String EXERCISE_SELECTED = "com.sdd.fitnessleague.MESSAGE";
	public final static String WORKOUT_SELECTED = "com.sdd.fitnessleague.WORKOUT";
	
	/**
	 * Function that gets called when the screen is created
	 * The function checks fetches user information
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// Set up the main view
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		addItemsOnSpinner();
		addListenerOnButton();
			
		// Collect facebook information
		Session session = Session.getActiveSession();
		if (session != null && session.isOpened()) {
			Request.newMeRequest(session, new Request.GraphUserCallback() {

				@Override
				public void onCompleted(GraphUser user, Response response) {
					if (user != null) {
						TextView welcome = (TextView) findViewById(R.id.greeting_message);
						welcome.setText("Welcome " + user.getFirstName() + "!");
						uId = user.getId();
						addItemsOnSpinner();
						
						try{
							UserManagerTask emt = new UserManagerTask();
							ArrayList<String> result = emt.execute("GetProfile", user.getId()).get();
							//If not profile is found, go to profile page and create profile
							if (result == null || result.size() == 0){
								Intent intent = new Intent(MainActivity.this, UserProfileActivity.class);
						        startActivity(intent);
						        
						        //Add user to the experience table
						        LevelManagerTask lmt = new LevelManagerTask();
						        lmt.execute("addNewUser", user.getId());
						        
							}
							//Check for last user activity
							else {
								LevelManagerTask lmt = new LevelManagerTask();
								result = lmt.execute("getLastActivity", user.getId()).get();
								if (result.size() != 0 || result != null){
									SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
									Date lastActivity = sdf.parse(result.get(0));
									Date today = new Date();

									Calendar cal = Calendar.getInstance();
							        cal.setTime(today);
							        cal.add(Calendar.MONTH, -1);
							        Date lastMonth = cal.getTime();
							        sdf.format(lastMonth);
							        
							        //If user haven't logged in for a month then decrease level
							        if  (lastActivity.before(lastMonth))
							        {
							        	int currentLevel = 0;
							        	LevelManagerTask lmt2 = new LevelManagerTask();
										result = lmt2.execute("getLevel", user.getId()).get();
										if (result.size() != 0 || result != null){
											currentLevel = Integer.parseInt(result.get(0));								
										}
										
										//Decrease level if it's not level 1
										if(currentLevel > 1) {
											String[] newLevel = new String[]{
												"setLevel",
												user.getId(),
												Integer.toString(currentLevel - 1)
											};
											LevelManagerTask lmt3 = new LevelManagerTask();
											lmt3.execute(newLevel);
											
											String[] newExp = new String[]{
												"setExp",
												user.getId(),
												Integer.toString(0)
											};
											LevelManagerTask lmt4 = new LevelManagerTask();
											lmt4.execute(newExp);
										}
							        }
									
								}
							}
						}
						catch (Exception e) {
							e.printStackTrace();
						}
					}
					else
					{
						TextView welcome = (TextView) findViewById(R.id.greeting_message);
						welcome.setText("No user found!");
					}
				}				
			}).executeAsync();
		}
		else {
	    	finish();
        }
	}
	
	@Override
	/**
	 *  Do not display anything in the action bar
	 */
	protected void setupActionBar() {
	}
	

	/**
	 * Starts the intent for the Exercise List -- activated by button
	 * @param view the current view
	 */
	public void viewExerciseList(View view) {
		Intent intent = new Intent(this, ExerciseListActivity.class);
		startActivity(intent);
	}
	
	/**
	 * Starts activity to add a new workout
	 * @param view the current view
	 */
	public void goToAddWorkout(View view) {
		Intent intent = new Intent(this, AddWorkout.class);
		startActivity(intent);
	}
	
	/**
	 * Starts the QR Code scanner -- activated by button
	 * @param v the current view
	 */
	public void launchQRScanner(View v) {
        if (isCameraAvailable()) {
            Intent intent = new Intent(this, ZBarScannerActivity.class);
            intent.putExtra(ZBarConstants.SCAN_MODES, new int[]{Symbol.QRCODE});
            startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
        } else {
            Toast.makeText(this, "Rear Facing Camera Unavailable", Toast.LENGTH_SHORT).show();
        }
    }
	
	/**
	 * Checks if there is a camera available
	 * @return true if the camera is available on the device
	 */
	public boolean isCameraAvailable() {
        PackageManager pm = getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }
	
	/**
	 * Called when the qr scanner returns due to a successful scan or the user hitting the back button
	 * @param requestCode The type of scan that occurred
	 * @param resultCode The type of result (success or failure)
	 * @param data The data read by the scanner
	 */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ZBAR_SCANNER_REQUEST:
            case ZBAR_QR_SCANNER_REQUEST:
                if (resultCode == RESULT_OK) {
               		Intent intent = new Intent(this, ExerciseActivity.class);
                    intent.putExtra(EXERCISE_SELECTED, data.getStringExtra(ZBarConstants.SCAN_RESULT));
                    startActivity(intent);
                } else if(resultCode == RESULT_CANCELED && data != null) {
                    String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
                    if(!TextUtils.isEmpty(error)) {
                        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }
	
    /**
     * Starts the intent to view the logs for the user
     * @param view the current view
     */
	public void showLog(View view) {
		Intent intent = new Intent(this, LogActivity.class);
		startActivity(intent);
	}
	
	//Dropdown variables
	 private Spinner spinner2;
	 private Button button;

	  /**
	   * Add items into spinner dynamically from the database
	   */
	  public void addItemsOnSpinner() {

		//final ListView listview = (ListView) findViewById(R.id.exercise_list_view);
		final ArrayList<String> list = new ArrayList<String>();
		spinner2 = (Spinner) findViewById(R.id.spinner2);
 
    	try{
			// Execute command to fetch list of all exercises
	    	WorkoutManagerTask wmt = new WorkoutManagerTask();
	    	ArrayList<String> result = wmt.execute("listAll", uId).get();
	    	list.add("Choose a workout");
	    	if (result == null){
	    		list.add ("Add new workout");
	    	}
	    	else{
	    		// Add all to the current view
	    		list.addAll(result);
	    		list.add("Add new workout");
		    }
	    }catch (ExecutionException e){
        	e.printStackTrace();
		}catch (InterruptedException e){
        	e.printStackTrace();
		}
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner2.setAdapter(dataAdapter);
	}

	  /**
	   * Reads the value of the spinner and performs the specified action
	   */
	  public void addListenerOnButton() {
		spinner2 = (Spinner) findViewById(R.id.spinner2);
		button = (Button) findViewById(R.id.button);

		button.setOnClickListener(new OnClickListener() {

		  @Override
		  public void onClick(View view) {
		    if(String.valueOf(spinner2.getSelectedItem()) == "Add new workout"){
		    	goToAddWorkout(view);
		    }
		    else if (String.valueOf(spinner2.getSelectedItem()) == "Choose a workout"){
		    	Toast.makeText(MainActivity.this,"Please select a workout", Toast.LENGTH_SHORT).show();
		    }
		    // If a workout has been selected
		    else{ 
			  String workoutName = String.valueOf(spinner2.getSelectedItem());
			  Intent intent = new Intent(MainActivity.this, WorkoutActivity.class);
		      intent.putExtra(MainActivity.WORKOUT_SELECTED, workoutName);
		      startActivity(intent);
		    }
		  }
		});
	  }
}
